﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerashake : MonoBehaviour
{
	public Transform camTransform;
	public float shakeDuration = 5f;
	public float shakeAmount = 0f;
	public float decreaseFactor = 1.0f;

	//private Button buttonScript;

	Vector3 originalPos;

	private void Awake()
	{
	
		if (camTransform == null)
		{
			camTransform = GetComponent(typeof(Transform)) as Transform;
		}
	}

	private void OnEnable()
	{
		originalPos = camTransform.localPosition;
	}

	void Update()
	{
		
			camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
		
		
	}
}

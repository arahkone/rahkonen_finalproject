﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
	public bool on;
	public Image buttonbackground;

	public Color oncolor;

	private AudioSource source;
	public AudioClip sound;

    public Camerashake cshake;
    public ParticleSystem system;

	public void Awake()
	{
		source = GetComponent<AudioSource>();
	}
    public void toggle()
	{
		on = !on;
		if (on)
		{
			buttonbackground.color = oncolor;
			source.Play();
            cshake.shakeAmount += .6f;
            system.Play();
		}
		else
		{
			buttonbackground.color = Color.white;
			source.Stop();
            cshake.shakeAmount -= .6f;
            system.Pause();
        }
	}
}
